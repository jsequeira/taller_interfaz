/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tallerinterfaz;
import javax.swing.JOptionPane;
import javax.swing.tree.*;
/**
 *
 * @author estudiante
 */
public class JTree extends javax.swing.JDialog {

    DefaultMutableTreeNode Titulo=null;
    DefaultTreeModel modelo=null;
    
    
    
    
    public JTree(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        jTree1.setModel(cargarArbol());
        setLocationRelativeTo(null); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        Agregar = new javax.swing.JButton();
        Remover = new javax.swing.JButton();
        Informacion = new javax.swing.JButton();
        Modificar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jScrollPane1.setViewportView(jTree1);

        Agregar.setText("Agregar");
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });

        Remover.setText("Remover");
        Remover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoverActionPerformed(evt);
            }
        });

        Informacion.setText("Informacion");
        Informacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InformacionActionPerformed(evt);
            }
        });

        Modificar.setText("Modificar");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });
        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(Agregar)
                        .addGap(10, 10, 10)
                        .addComponent(Remover)
                        .addGap(18, 18, 18)
                        .addComponent(Informacion)
                        .addGap(18, 18, 18)
                        .addComponent(Modificar)
                        .addGap(0, 14, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Agregar)
                    .addComponent(Remover)
                    .addComponent(Informacion)
                    .addComponent(Modificar))
                .addGap(40, 40, 40))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed

        
        String cadena=JOptionPane.showInputDialog(this,"Ingrese un departamento");
        DefaultMutableTreeNode parentNodo=null;
        TreePath parentPath=jTree1.getSelectionPath();
        if(parentPath==null){
        
      }else{
        parentNodo=(DefaultMutableTreeNode)parentPath.getLastPathComponent();
       }
        DefaultMutableTreeNode child=new DefaultMutableTreeNode(cadena);
        modelo.insertNodeInto(child,parentNodo,parentNodo.getChildCount());

        // TODO add your handling code here:
    }//GEN-LAST:event_AgregarActionPerformed

    private void RemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoverActionPerformed
DefaultMutableTreeNode parentNode=null;
TreePath currentSelection=jTree1.getSelectionPath();
if(currentSelection!=null){
parentNode=(DefaultMutableTreeNode)
currentSelection.getLastPathComponent();
DefaultTreeModel model=((DefaultTreeModel)jTree1.getModel());
model.removeNodeFromParent(parentNode);
        }


// TODO add your handling code here:
    }//GEN-LAST:event_RemoverActionPerformed

    private void InformacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InformacionActionPerformed
DefaultMutableTreeNode node = (DefaultMutableTreeNode)
jTree1.getLastSelectedPathComponent();
if (node == null) { //Nada seleccionado
 return;
}
Object nodeInfo = node.getUserObject();
JOptionPane.showMessageDialog(this, nodeInfo.toString());
    }//GEN-LAST:event_InformacionActionPerformed

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed
DefaultMutableTreeNode node = (DefaultMutableTreeNode)
jTree1.getLastSelectedPathComponent();
if (node == null) {//Nada seleccionado
 return;
}
Object nodeInfo = node.getUserObject();

String cadena = JOptionPane.showInputDialog(this, "Ingrese el nuevonombre",nodeInfo.toString());
TreePath currentSelection = jTree1.getSelectionPath();
if (currentSelection != null) {
 node = (DefaultMutableTreeNode) currentSelection.getLastPathComponent();
node.setUserObject(cadena);
 DefaultTreeModel model = ((DefaultTreeModel) jTree1.getModel());
 model.nodeChanged(node);
}         // TODO add your handling code here:
    }//GEN-LAST:event_ModificarActionPerformed

    public DefaultTreeModel cargarArbol(){
   
 DefaultMutableTreeNode categorias = null;
 DefaultMutableTreeNode subcategoria = null;
 Titulo = new DefaultMutableTreeNode("Centro de Administración");
 modelo = new DefaultTreeModel(Titulo);
 categorias = new DefaultMutableTreeNode("Adm. Redes");
 subcategoria = new DefaultMutableTreeNode("Dept. de planificación");
 modelo.insertNodeInto(categorias, Titulo, 0);
 modelo.insertNodeInto(subcategoria, categorias, 0);
 categorias = new DefaultMutableTreeNode("Adm. de Laboratorios");
 subcategoria = new DefaultMutableTreeNode("Dept. de Tecnologia");
 modelo.insertNodeInto(categorias, Titulo, 1); 
 modelo.insertNodeInto(subcategoria, categorias, 0);
 categorias = new DefaultMutableTreeNode("Dept. de Investigación");
 subcategoria = new DefaultMutableTreeNode("Dept. A");
 modelo.insertNodeInto(categorias, Titulo, 2);
 modelo.insertNodeInto(subcategoria, categorias, 0);
 return modelo; 
 
    }
    
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JTree.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JTree.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JTree.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JTree.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JTree dialog = new JTree(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JButton Informacion;
    private javax.swing.JButton Modificar;
    private javax.swing.JButton Remover;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree1;
    // End of variables declaration//GEN-END:variables
}
